﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PDE_WPF
{
    class StartCommand : ICommand
    {
        Model model;


        public StartCommand(Model _model)
        {

            this.model = _model;
        }


        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
        
        }
    }
}

