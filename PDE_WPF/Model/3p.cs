﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Numerics;

namespace PDE_WPF
{
    public class UkladTrojprzekatniowy
    {
        public static int rozw3p(int N, Complex[] A, Complex[] B, Complex[] C, Complex[] D, Complex[] f)
        {
            if (A[N - 1].Magnitude != 0 || C[0].Magnitude != 0)
                Console.Error.WriteLine("*** Uwaga! Wartosci przekatnej A lub C poza zakresem sa niezerowe!");

            //MACIERZE DO PRZESKALOWANIA
            Complex[] X = new Complex[N];
            Complex[] Y = new Complex[N];

            //JEDNORODNE => ROZWIAZANIE TRYWIALNE
            double czy_zero = 0;
            for (int i = 0; i < N; i++) czy_zero += D[i].Magnitude * D[i].Magnitude;
            if (czy_zero == 0)
            {
                Console.Error.WriteLine("*** Ostrzezenie: Rownanie jednorodne => Rozwiazanie trywialne");
                return 1;
            }

            //PRZESKALOWYWANIE MACIERZY X,Y
            //gdy i=N: A=0
            X[N - 1] = -C[N - 1] / B[N - 1];
            Y[N - 1] = D[N - 1] / B[N - 1];
            for (int i = N - 1; i > 0; i--)
            {
                Complex mianownik = A[i] * X[i] + B[i];
                if (mianownik == 0)
                {
                    Console.Error.WriteLine("*** Dzielenie przez zero => Uklad sprzeczny");
                    throw new Exception("Uklad sprzeczny");
                }

                X[i - 1] = -C[i] / mianownik;
                Y[i - 1] = (D[i] - A[i] * Y[i]) / mianownik;
            }

            //REKURENCYJNE ROZWIAZYWANIE Z POMOCA PRZESKALOWANYCH MACIERZY
            f[0] = (D[0] - A[0] * Y[0]) / (A[0] * X[0] + B[0]);
            for (int i = 0; i < N - 1; i++) f[i + 1] = X[i] * f[i] + Y[i];

            return 0;
        }
    }
}
