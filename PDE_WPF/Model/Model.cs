﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using OxyPlot;
using System.ComponentModel;
using System.Windows.Threading;
using System.Windows;
using System.Globalization;
using OxyPlot.Wpf;
using System.Collections.ObjectModel;

namespace PDE_WPF
{
    class Model
    {
        #region Pola Klasy


        bool _blokada = false;
        double _nt = 10000;
        double _xmax = 10;
        double _xmin = -10;
        double _dt = 0.001;
        double _h = 1; //h kreslone
        double _m = 1; //masa elektronu
        //Gauss
        double _a = 1;
        double _x0 = -5;
        double _k = 0;
        double _czasT = 0;
        int _nx = 1001;
        Complex S2;
        double dx;

        string _komunikat = String.Empty;
        string _komunikat2 = String.Empty;
        IList<DataPoint> _polozenie;
        IList<DataPoint> _potencjal;
        
        ModelWidok MW;
        List<double> gdziePotDodanyPlus;
        List<double> gdziePotDodanyMinus;
        ObservableCollection<DaneDat> _dat;
        int licznik;
        int funkcja_potencjalu = 0;
        private Dispatcher dispatcher = Dispatcher.CurrentDispatcher;
        static readonly IFormatProvider formatProvider = CultureInfo.InvariantCulture;
        #endregion


        #region Konstruktory
        public Model(ModelWidok m)
        { MW = m; licznik = 0;
            gdziePotDodanyPlus = new List<double>();
            gdziePotDodanyMinus = new List<double>();
        }
        #endregion

        #region GetSet
        public ObservableCollection<DaneDat> Dat
        {
            get { return _dat; }
        }
       public bool blokada
        {
            set { _blokada = value; }
        }
        public string CzasTitle
        { get { return "t=" + _czasT.ToString(); } }
        public int FunkcjaPotencjalu
        {
            get { return funkcja_potencjalu; }
            set { funkcja_potencjalu = value; }
        }
        public IList<DataPoint> ListaFunkcjaFalowa { get { return _polozenie; } }
        public IList<DataPoint> ListaPotencjal { get { return _potencjal; } }
        public double nt
        {
            get { return _nt; }
            set { _nt = value; }
        }
        public double xmin
        {
            get { return _xmin; }
            set { _xmin = value; }
        }
        public double xmax
        {
            get { return _xmax; }
            set { _xmax = value; }
        }
        public int nx
        {
            get { return _nx; }
            set { _nx = value; }
        }
        public double dt
        {
            get { return _dt; }
            set { _dt = value; }
        }
        public double h
        {
            get { return _h; }
            set { _h = value; }
        }
        public double m
        {
            get { return _m; }
            set { _m = value; }
        }
        public double a
        {
            get { return _a; }
            set { _a = value; }
        }
        public double x0
        {
            get { return _x0; }
            set { _x0 = value; }
        }
        public double kG
        {
            get { return _k; }
            set { _k = value; }
        }

       
        public string komunikat { get { return _komunikat; }
            set { _komunikat = value; } }

        public string komunikat2
        {
            get { return _komunikat2; }
            set { _komunikat2 = value; }
        }
        #endregion

        #region Metody
        double _dx()
        {
            dx = (_xmax - _xmin) / (_nx - 1);
            return dx;
        }
        double t(int i)
        {
            return i * _dt;
        }
        double x(int i)
        {
            return _xmin + i * dx;
        }
     
        Complex gauss(double x)
        {

            double xn = (x - _x0) / (2 * _a);
            return Complex.Exp(-xn * xn + Complex.ImaginaryOne * kG * x);
        }


        public async void DodajPotencjal(double value, bool p)
        {
            _blokada = true;

            if (p)
                lock (gdziePotDodanyMinus) gdziePotDodanyMinus.Add(value);
            else
            {
                lock (gdziePotDodanyPlus) gdziePotDodanyPlus.Add(value);
            }
            await Task.Delay(400);
            _blokada = false;
        }
    public async void ResetujPotencjal()
        {
            lock (gdziePotDodanyMinus) gdziePotDodanyPlus = new List<double>();
            lock (gdziePotDodanyPlus) gdziePotDodanyMinus = new List<double>();
            await Task.Delay(100);
            RysPotencjal();
           
        }
    double dodanyGrzbiet(double x, double k)
        {
            double y;     
            y = k-x;
            return (y*y*-1)+1;
        }
        public double DodatkiPotencjalu(double x)
        {
            double suma = 0;
            lock (gdziePotDodanyPlus)
            {
             
                if (gdziePotDodanyPlus.Count() > 0)
                {

                    foreach (double k in gdziePotDodanyPlus)
                    {
                        if (x < k + 1 & x > k - 1)
                        {
                            suma += dodanyGrzbiet(x, k);

                        }
                    }
                }
            }
            lock (gdziePotDodanyMinus)
                if (gdziePotDodanyMinus.Count() > 0)
                {

                    foreach (double k in gdziePotDodanyMinus)
                    {
                        if (x < k + 1 & x > k - 1)
                        {
                            suma -= dodanyGrzbiet(x, k);

                        }
                    }
                }
            return suma;
        }

    


           



        Complex V(double x)
        {
            switch (funkcja_potencjalu)
            {
                case 0: //x*x
                    {
                       
                         return x * x+DodatkiPotencjalu(x);
                       
                    }

                case 2: //ABs(x)
                    {
                        // return ((1 / 2 * Math.PI) * Math.Exp(-(x * x) / 2));
                        return Math.Abs(x) + DodatkiPotencjalu(x);
                    }
                case 1: //x
                    {
                        // return (1 / 2 * Math.PI * Math.Exp(-(x * x) / 2));
                        return x + DodatkiPotencjalu(x);
                    }

                case 3: //prostokatny
                    {
                        double a = xmin + (xmax + Math.Abs(xmin)) / 3;
                        if (x < a) return xmax / 2 + DodatkiPotencjalu(x);
                        if (x > a & x < 2*Math.Abs(a)) return xmin / 2 + DodatkiPotencjalu(x);
                        else return xmax / 2 + DodatkiPotencjalu(x);
                    }

                case 4:
                    {
                        return Math.Sin(x) + DodatkiPotencjalu(x);
                    }
                case 5:
                    {
                        return Math.Cos(x) + DodatkiPotencjalu(x);
                    }
                case 6: //staly
                    {
                        return 4+DodatkiPotencjalu(x);
                    }



                default:
                    return x * x;
            }

        }
        Complex _S2()
        {
            S2 = -_h * _h / 2 / _m / (Complex.ImaginaryOne * _h);
            return S2;
        }
        Complex S0(double x)
        {
            return V(x) / (Complex.ImaginaryOne * _h);
        }

        #endregion

        
        async void tdse()
        {

            if (!_blokada)
            {
                licznik++;
                Complex[] A = new Complex[_nx];
                Complex[] B = new Complex[_nx];
                Complex[] C = new Complex[_nx];
                Complex[] D = new Complex[_nx];
                Complex[] Psi = new Complex[_nx];


                double norma = 0;


                for (int i = 0; i < _nx; ++i)
                {
                    Psi[i] = gauss(x(i));
                    norma += Psi[i].Magnitude * Psi[i].Magnitude;
                }


                norma *= dx;
                norma = Math.Sqrt(norma);
                for (int i = 0; i < _nx; ++i)
                {
                    Psi[i] /= norma;
                }


                for (int k = 0; k < _nt; k++)
                {
                    _czasT = k;
                    if (k > 0)
                    {

                        for (int i = 0; i < _nx; ++i)
                        {
                            //obliczenia macierzy trojdiagonalnej
                            A[i] = -S2 / dx;
                            B[i] = 2 * dx / dt + 2 * S2 / dx - dx * S0(x(i));
                            C[i] = -S2 / dx;
                            D[i] = Psi[i] * (2 * dx / dt - 2 * S2 / dx + dx * S0(x(i)));

                            //warunki brzegowe
                            if (i < _nx - 1) D[i] += Psi[i + 1] * (S2 / dx);
                            if (i > 0) D[i] += Psi[i - 1] * S2 / dx;

                            A[_nx - 1] = 0;
                            C[0] = 0;


                        }

                        //rozwiazywanie macierzy trojprzekatniowej
                        try
                        {
                            UkladTrojprzekatniowy.rozw3p(_nx, A, B, C, D, Psi);
                        }
                        catch (Exception exc)
                        {
                            _komunikat += ("**** Blad ****\t" + exc.Message);
                            System.Environment.Exit(1);
                        }
                    }

                    //obliczqanie wielkosci zaleznych od czasu
                    norma = 0;

                    double polozenie = 0;

                    double psiI = 0, psiR = 0;


                    if (k % 5 == 0)
                    {
                        ListaFunkcjaFalowa.Clear();
                    }
                    for (int i = 0; i < _nx; ++i)
                    {
                        double _norma = Psi[i].Magnitude * Psi[i].Magnitude;
                        norma += _norma;
                        polozenie += x(i) * _norma;

                        if (k % 5 == 0)
                        {

                            psiI = Psi[i].Imaginary * Psi[i].Imaginary;
                            psiR = Psi[i].Real * Psi[i].Real;
                            _czasT = k;
                            ListaFunkcjaFalowa.Add(new DataPoint(x(i), psiI + psiR));
                        }

                    }
                    if (k % 5 == 0)
                    {
                        MW.UpdateWykresFalowa();
                        await Task.Delay(100);
                        if (k % 100 == 0) MW.UpdateTxt();
                    }

                    norma *= dx;
                    polozenie *= dx;


                    if (k % 10 == 0)
                    {
                        dispatcher.Invoke(() => _dat.Add(new DaneDat(k,t(k), norma, polozenie)));
                       // komunikat += (k + "\t" + t(k) + "\t\t" + norma + "\t\t" + polozenie.ToString() + "\n");
                        komunikat2 = (k + "\t" + t(k) + "\t\t" + norma + "\t\t" + polozenie.ToString() + "\n");
                    }



                    if(k==_nt-1)   MW.Blokada = true;
                }
                MW.Update();
                zapiszPsi(Psi, licznik);
            }
            else await Task.Delay(100);
        }


     


        public void RysPotencjal()
        {
            _potencjal = new List<DataPoint>();
            ListaPotencjal.Clear();
            double pot = 0;
            _dx();
            for (int i = 0; i < _nx; ++i)
            {
                pot = V(x(i)).Real + V(x(i)).Imaginary;
                pot *= dx;
                ListaPotencjal.Add(new DataPoint(x(i), pot)); pot = 0;
            }
            MW.UpdateWykresPot();
           
        }

        public void start()
        {
            
            _komunikat = "Start! \n";
            _dx();
            _S2();
            
            _polozenie = new List<DataPoint>();
            _potencjal = new List<DataPoint>();
            _dat = new ObservableCollection<DaneDat>();
            _dat.Clear();
            ListaPotencjal.Clear();
            ListaFunkcjaFalowa.Clear();

            _komunikat += "\nnt: \t" + _nt;
            _komunikat += "\nxmax: \t" + _xmax;
            _komunikat += "\nxmin: \t" + _xmin;
            _komunikat += "\ndt: \t" + dt;
            _komunikat += "\nh: \t" + h;
            _komunikat += "\nm: \t" + m;           
            _komunikat += "\na: \t" + a;
            _komunikat += "\nx0: \t" + x0;
            _komunikat += "\nk: \t" + kG;
            _komunikat += "\nnx: \t" + nx;           
            _komunikat += "\ndx: \t" + dx;
            _komunikat += "\nFunkcja potencjału: \t" + (funkcja_potencjalu+1);
            _komunikat += "\n\n\n";
            MW.UpdateTxt();


            RysPotencjal();
                tdse();

          
            MW.Update();

         
        }

         void zapiszPsi(Complex[] Psi, int k)
        {
            List<string> linie = new List<string>();
            for (int i = 0; i < _nx; ++i)
            {
                string s = x(i).ToString(formatProvider) + "\t" + Psi[i].Real.ToString(formatProvider) + "\t" + Psi[i].Imaginary.ToString(formatProvider) + "\t" + (Psi[i].Magnitude * Psi[i].Magnitude).ToString(formatProvider);
                linie.Add(s);
            }
            System.IO.File.WriteAllLines("Psi" + k.ToString() + ".dat", linie);
        }



    }
}
