﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using OxyPlot;
using System.Windows;
using System.Windows.Threading;
using OxyPlot.Series;
using System.Collections.ObjectModel;
using System.Windows.Controls;

namespace PDE_WPF
{
    public class ModelWidok : INotifyPropertyChanged
    {

        #region Pola Klasy
        Model model;
        MainWindow MW;
        bool _odejmuj = false;
        bool _blokada = true;
        Task startT;
        //  Wizualizacja win;
        #endregion


        #region Konstruktory
        public ModelWidok(MainWindow mw)
        {
            model = new Model(this);
            MW = mw;
        }

        #endregion

        #region GetSet
        public ObservableCollection<DaneDat> Dat
        { get { return model.Dat; } }
        public bool Blokada
        {
            set { _blokada = value; }
            get { return _blokada; }
        }
        

        public string CzasTitle
        { get { return model.CzasTitle; } }
        public int FunkcjaPotencjalu
        {
            get { return model.FunkcjaPotencjalu; }

            set
            {
                model.FunkcjaPotencjalu = value;
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("FunkcjaPotencjalu"));
            }
        }
        public bool Odejmuj
        {
            get
            {
                return _odejmuj;
            }
            set
            {
                _odejmuj = value;
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("Odejmuj"));
            }
        }
        public double a
        {
            get
            {
                return model.a;
            }
            set
            {
                model.a = value;
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("a"));
            }
        }
        public double k
        {
            get
            {
                return model.kG;
            }
            set
            {
                model.kG = value;
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("k"));
            }
        }
        public double x0
        {
            get
            {
                return model.x0;
            }
            set
            {
                model.x0 = value;
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("x0"));
            }
        }
        public double m
        {
            get
            {
                return model.m;
            }
            set
            {
                model.m = value;
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("m"));
            }
        }
        public double h
        {
            get
            {
                return model.h;
            }
            set
            {
                model.h = value;
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("h"));
            }
        }
        public double nt
        {
            get
            {
                return model.nt;
            }
            set
            {
                model.nt = value;
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("nt"));
            }
        }
        public double dt
        {
            get
            {
                return model.dt;
            }
            set
            {
                model.dt = value;
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("dt"));
            }
        }
        public double xmax
        {
            get
            {
                return model.xmax;
            }
            set
            {
                model.xmax = value;
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("xmax"));
            }
        }
        public double xmin
        {
            get
            {
                return model.xmin;
            }
            set
            {
                model.xmin = value;
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("xmin"));
            }
        }
        public int nx
        {
            get
            {
                return model.nx;
            }
            set
            {
                model.nx = value;
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("nx"));
            }
        }
        public string komunikat
        {
            get
            {

                return model.komunikat;
            }
            set
            {
                model.komunikat = value;

                PropertyChanged(this, new PropertyChangedEventArgs("komunikat"));
            }
        }
        public string komunikat2
        {
            get
            {

                return model.komunikat2;
            }
            set
            {
                model.komunikat2 = value;

                PropertyChanged(this, new PropertyChangedEventArgs("komunikat2"));
            }
        }
        public IList<DataPoint> Polozenie
        {
            get
            {
                return model.ListaFunkcjaFalowa;
            }
        }
        public IList<DataPoint> Potencjal
        {
            get
            {
                return model.ListaPotencjal;
            }
        }
        #endregion


        #region Komendy (Command)

        private ICommand mouseDown = null;
        public ICommand MouseDown
        {
            get
            {

                if (mouseDown == null) mouseDown = new MvvmCommand(Myszka, (object parameter) => { return true; });
                return mouseDown;
            }
        }
        private void Myszka(object parameter)
        {
            lock (this)
            {
                model.blokada = true;
                Point mousePos = Mouse.GetPosition((IInputElement)parameter);
                double dx = (xmax - xmin) / (nx - 1);

                double klik = mousePos.X ;
                double ktory = 0;
                if (klik >= 0 & klik <= 850)
                {

                    ktory = xmin + klik * nx / 850 * dx;
                    model.DodajPotencjal(ktory, _odejmuj);
                    model.RysPotencjal();
                }
            }
        }



        private ICommand rysujCommand = null;
        public ICommand RysujCommand
        {
            get
            {

                if (rysujCommand == null) rysujCommand = new MvvmCommand(rysuj, (object parameter) => { return true; });
                return rysujCommand;
            }
        }
        private async void rysuj(object parameter)
        {

            model.RysPotencjal();




        }


        private ICommand resetujCommand = null;
        public ICommand ResetujCommand
        {
            get
            {

                if (resetujCommand == null) resetujCommand = new MvvmCommand(resetuj, (object parameter) => { return true; });
                return resetujCommand;
            }
        }
        private void resetuj(object parameter)
        {

            model.ResetujPotencjal();
        }

        private ICommand startCommand = null;
        public ICommand StartCommand
        {
            get
            {

                if (startCommand == null) startCommand = new MvvmCommand(run, (object parameter) => { return true; });
                return startCommand;
            }
        }
        private async void run(object parameter)
        {
       
                _blokada = false;
                PropertyChanged(this, new PropertyChangedEventArgs("Blokada"));
           

            if (startT != null)
            {
              //  if (startT.Status.Equals(TaskStatus.Running)) Task.WaitAll(startT);
            }
           startT=Task.Run(() =>
           {
               model.start();
             
           });
          //  await startT;
            startT.Wait();
            
          
           


        }


   


        #endregion

        #region UpdateUI

        public async void UpdateWykresFalowa()
        {
            await Task.Run(() =>
            {
                PropertyChanged(this, new PropertyChangedEventArgs("Polozenie"));
                PropertyChanged(this, new PropertyChangedEventArgs("CzasTitle"));
                Application.Current.Dispatcher.Invoke(new Action(() => { MW.wykres1.InvalidatePlot(true); }));
            });
        }

        public async void UpdateWykresPot()
        {
            await Task.Run(() =>
            {
                PropertyChanged(this, new PropertyChangedEventArgs("Potencjal"));
                Application.Current.Dispatcher.Invoke(new Action(() => { MW.wykres2.InvalidatePlot(true); }));
            });
        }

        public async void Update()
        {
            await Task.Run(() =>
            {
                UpdateTxt(); UpdateWykresFalowa(); UpdateWykresPot();
                PropertyChanged(this, new PropertyChangedEventArgs("Blokada"));
            });
        }

        public async void UpdateTxt()
        {
            await Task.Run(() =>
            {
                PropertyChanged(this, new PropertyChangedEventArgs("Dat"));
                PropertyChanged(this, new PropertyChangedEventArgs("komunikat"));
                PropertyChanged(this, new PropertyChangedEventArgs("komunikat2"));
                //MW.dataGrid.Upda
            });
        }

        #endregion




        public event PropertyChangedEventHandler PropertyChanged;
    }

    #region Pomocnicze 

    public class Funkcja
    {
        public String Nazwa { get; set; }

        public Funkcja(String Nazwa)
        {
            this.Nazwa = Nazwa;
        }

    }

    public class Funkcje : ObservableCollection<Funkcja>
    {
        public Funkcje()
        {
            Add(new Funkcja("x*x"));
            Add(new Funkcja("x"));
            Add(new Funkcja("Abs(x)"));
            //Add(new Funkcja("Gauss"));
            Add(new Funkcja("Prostokątna"));
            Add(new Funkcja("Sin"));
            Add(new Funkcja("Cos"));
            Add(new Funkcja("Stały=4"));
        }

    }

    public class DaneDat
    {
        double t; double n, p; int k;

        public DaneDat(int k, double t, double n, double p)
        {
            this.k = k;
            this.t = t;
            this.n = n;
            this.p = p;
        }
        public int Iteracja
        {
            get { return k; }
        }
        public double Czas
        { get { return t; } }

        public double Norma
        {
            get { return n; }
        }
        public double Położenie
        {
            get { return p; }

        }
    }
    #endregion
}