﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PDE_WPF
{
    /// <summary>
    /// Interaction logic for Wizualizacja.xaml
    /// </summary>
    public partial class Wizualizacja : Window
    {
        ModelWidok MW;
        public Wizualizacja(ModelWidok mw)
        {
            InitializeComponent();
            MW = mw;
            this.DataContext = mw;

        }
    
    }
}
